import turtle
import time # to delay the turtles, as they seem to be too fast
import random

# screen
window = turtle.Screen()
window.title("Snake Gamer")
window.bgcolor("green")
window.setup(width=600,height=600)
window.tracer(0) # turns off the screen updates

# snake head
head = turtle.Turtle()
head.speed(0) # animation speed of the turtle
head.shape("square")
head.color("black")
head.penup()
head.goto(0,0)
head.direction = "stop"

# snake body piece

segments = []

# snake food
food = turtle.Turtle()
food.speed(0)
food.shape("circle")
food.color("red")
food.penup()
food.goto(0,100)

# functions
def go_up():
    if head.direction != "down":
        head.direction = "up"
def go_down():
    if head.direction != "up":
        head.direction = "down"
def go_right():
    if head.direction != "left":
        head.direction = "right"
def go_left():
    if head.direction != "right":
        head.direction = "left"

def movement():
    if head.direction == 'up':
        y = head.ycor()
        head.sety(y + 20)
    elif head.direction == 'down':
        y = head.ycor()
        head.sety(y - 20)
    elif head.direction == 'right':
        x = head.xcor()
        head.setx(x + 20)
    elif head.direction == 'left':
        x = head.xcor()
        head.setx(x - 20)

# key bindings
window.listen()
window.onkeypress(go_up,"w")
window.onkeypress(go_down,"s")
window.onkeypress(go_right,"d")
window.onkeypress(go_left,"a")

# main game loop
while True: # repeats forever
    window.update()

    # checking for collisions with the border
    if head.xcor() > 290 or head.xcor() < -290 or head.ycor() > 290 or head.ycor() < -290:
        time.sleep(1)
        head.goto(0,0)
        head.direction = "stop"

        # moving them off the screen, because segments = [0] does not work as intended
        for segment in segments:
           segment.goto(1000,1000)
        segments.clear()

    # checking if the snake touched the food
    if head.distance(food) < 20:
        # move the food to a random place on screen
        x = random.randint(-290, 290)
        y = random.randint(-290, 290)
        food.goto(x, y)
        # add piece
        new_piece = turtle.Turtle()
        new_piece.speed(0)
        new_piece.shape("square")
        new_piece.color("gray")
        new_piece.penup()
        segments.append(new_piece)

    # move the end segments first in reverse order
    for index in range(len(segments)-1, 0, -1):
        x = segments[index-1].xcor()
        y = segments[index-1].ycor()
        segments[index].goto(x,y)
    # move segment 0 to where the head is
    if len(segments) > 0:
        x = head.xcor()
        y = head.ycor()
        segments[0].goto(x,y)

    movement()

    # checking for collisions of the head with the body
    for segment in segments:
        if segment.distance(head) < 20:
            time.sleep(1)
            head.goto(0, 0)
            head.direction = "stop"
            for segment in segments:
                segment.goto(1000, 1000)
            segments.clear()

    time.sleep(0.1)

